QT += core
QT += serialport
QT -= gui

CONFIG += c++11

TARGET = ModemAmrWriter
CONFIG += console
CONFIG -= app_bundle
CONFIG += release

TEMPLATE = app

SOURCES += main.cpp \
    atmodem.cpp

HEADERS += \
    atmodem.h

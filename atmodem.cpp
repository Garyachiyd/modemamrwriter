#include "atmodem.h"
#include <QDebug>
#include <QThread>
#include <iostream>

AtModem::AtModem()
{

}

QByteArray AtModem::waitLine(QString line){

    while(true){
        if(!serial.canReadLine()){
            serial.waitForReadyRead(-1);
        }
        while(serial.canReadLine()){
            QByteArray reply = serial.readLine();
            if(reply.contains(line.toLocal8Bit())){
               qDebug() << reply;
               return reply;
            }
        }
    }
}

QByteArray AtModem::waitLine()
{
    if(!serial.canReadLine()){
        serial.waitForReadyRead(-1);
        return serial.readLine();
    }
    else{
        return serial.readLine();
    }
}

int AtModem::waitChar(char waiting)
{
    char reply;
    while(true){
        if(!serial.getChar(&reply)){
            serial.waitForReadyRead(-1);
        }
        else{
            if(reply == waiting){
               return 0;
            }
        }
    }
}

int AtModem::init()
{
    std::vector<QSerialPortInfo> ports;
    int port = 0;
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()){
        ports.push_back(info);
        qDebug() << port << info.portName();
        port++;
    }

    qDebug() << "Chose serial port nomber";
    qDebug() <<"<<";
    std::cin >> port;
    qDebug() <<"selected" << port << "port" << ports[port].portName() ;

    serial.setPortName(ports[port].portName());
    serial.setBaudRate(115200);
    serial.open(QIODevice::ReadWrite);

    QThread::sleep(10);
    QByteArray reply;

    //reply = waitLine("Call Ready\r\n");
    serial.write("AT\r",strlen("AT\r"));
    reply = waitLine("OK\r\n");


    serial.write("AT+GMM\r",strlen("AT+GMM\r"));
    serial.waitForBytesWritten(-1);

    reply = waitLine("SIM");

    if(reply.contains("SIM900")){
        modemType = ModemType::SIM900;
    }else if (reply.contains("SIM800")){
        modemType = ModemType::SIM800;
    }

    reply = waitLine("OK\r\n");
    return 0;
}

int AtModem::initFilesystem()
{
    qDebug() << "initFilesystem";
    switch (modemType) {
    case ModemType::SIM800:{
        break;
    }
    case ModemType::SIM900:{
        serial.write("AT+CFSINIT\r",strlen("AT+CFSINIT\r"));
        serial.waitForBytesWritten(-1);
        waitLine("OK\r\n");
        break;
    }
    default:
        break;
    }
    return 0;
}

int AtModem::deinitFilesystem()
{
    qDebug() << "deinitFilesystem";
    switch (modemType) {
    case ModemType::SIM800:{
        break;
    }
    case ModemType::SIM900:{
        serial.write("AT+CFSTERM\r",strlen("AT+CFSTERM\r"));
        serial.waitForBytesWritten(-1);
        waitLine("OK\r\n");
        break;
    }
    default:
        break;
    }
    return 0;

}

QStringList AtModem::getFileList(QString filepath)
{
    qDebug() << "getFileList";
    QStringList filelist;
    switch (modemType) {
    case ModemType::SIM800:{
        serial.write("AT+FSLS=\\\r",strlen("AT+FSLS=\\\r"));
        serial.waitForBytesWritten(-1);
        while(true){
            QString reply =  waitLine();
            if(reply.contains("OK\r\n")){
                break;
            }else{
                filelist.push_back(reply);
            }
        }


        break;
    }
    case ModemType::SIM900:{
        serial.write("AT+CFSLIST\r",strlen("AT+CFSLIST\r"));
        serial.waitForBytesWritten(-1);
        while(true){
            QString reply =  waitLine();
            if(reply.contains("OK\r\n")){
                break;
            }else{
                filelist.push_back(reply);
            }
        }
        break;
    }
    default:
        break;
    }
    return filelist;
}

int AtModem::writeFile(QByteArray data, QString filepathInModem)
{
    QByteArray request;

    switch (modemType) {
    case ModemType::SIM800:{

        // create file
        request.push_back("AT+FSCREATE=\\");
        request.push_back(filepathInModem.toLocal8Bit());
        request.push_back("\r");
        serial.write(request.constData(), request.length());
        serial.waitForBytesWritten(-1);
        waitLine("OK\r\n");

        //write file
        request.push_back("AT+FSWRITE=\\");
        request.push_back(filepathInModem.toLocal8Bit());
        request.push_back(",0,");

        QString filesize;
        filesize =  QString::number(data.length());
        request.push_back(filesize.toLocal8Bit());

        request.push_back(",50000\r");
        qDebug() << "command: " << request ;
        serial.write(request,request.size());
        serial.waitForBytesWritten(-1);

        //waitLine(">\r\n");
        waitChar('>');

        break;
    }
    case ModemType::SIM900:{
        //write file
        request.push_back("AT+CFSWFILE=\"");
        request.push_back(filepathInModem.toLocal8Bit());
        request.push_back("\",0,");

        QString filesize;
        filesize =  QString::number(data.length());
        request.push_back(filesize.toLocal8Bit());

        request.push_back(",50000\r");
        qDebug() << "command: " << request ;
        serial.write(request,request.size());
        serial.waitForBytesWritten(-1);

        waitLine("CONNECT\r\n");

        break;
    }
    default:
        break;
    }

    serial.write(data,data.size());
    serial.waitForBytesWritten(-1);
    waitLine("OK\r\n");
return 0;
}

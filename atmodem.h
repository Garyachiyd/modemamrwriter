#ifndef ATMODEM_H
#define ATMODEM_H
#include <QFile>
#include <QObject>
#include <QtSerialPort/QSerialPortInfo>
#include <QtSerialPort/QSerialPort>

class AtModem
{
public:
   // Q_OBJECT

    AtModem();
    int init();
    int initFilesystem();
    int deinitFilesystem();
    QStringList getFileList(QString filepath);
    int writeFile(QByteArray data, QString filepathInModem);
private:
    enum ModemType{
        SIM800,
        SIM900
    };
    ModemType modemType;
    QSerialPort serial;
    QByteArray waitLine(QString line);
    QByteArray waitLine();
    int waitChar(char waiting);
};

#endif // ATMODEM_H

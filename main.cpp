#include <QCoreApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QDir>
#include <atmodem.h>
#include <iostream>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QCommandLineParser parser;

    parser.setApplicationDescription("App to write amr files dir in sim800 and sim 900 modems");
    parser.addHelpOption();
    parser.addVersionOption();

    // An option with a value
    QCommandLineOption targetDirectoryOption(QStringList() << "t" << "target-directory",
            QCoreApplication::translate("main", "Copy all source files into <directory>."),
            QCoreApplication::translate("main", "directory"));
    parser.addOption(targetDirectoryOption);

    // Process the actual command line arguments given by the user
    parser.process(a);

    QString sourseAmrPath;
    if(parser.isSet(targetDirectoryOption)){
      sourseAmrPath = parser.value(targetDirectoryOption);
    }
    else{
        sourseAmrPath = QDir::currentPath();
    }
    qDebug() << "sourseAmrPath: " << sourseAmrPath;
    QDir sourseAmrDir(sourseAmrPath);
    if(!sourseAmrDir.exists()){
        //error
    }

    QStringList filters;
    filters << "*.amr";
    sourseAmrDir.setNameFilters(filters);

    QStringList files = sourseAmrDir.entryList();

    AtModem atmodem;
    atmodem.init();

    atmodem.initFilesystem();



    foreach (const QString& str, files) {
        QFile file(str);
        file.open(QIODevice::ReadOnly);
        QByteArray fileData = file.readAll();

        atmodem.writeFile(fileData,file.fileName());

        qDebug()<< str;
    }

    QStringList filelist = atmodem.getFileList("\\");

    foreach (const QString& filename, filelist) {
        qDebug() << filename;
    }

    atmodem.deinitFilesystem();
    exit(1);
    return a.exec();
}
